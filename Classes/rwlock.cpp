//
//  rwlock.cpp
//  util
//
//  Created by 张琪 on 14-7-1.
//
//

#include "rwlock.h"

using namespace util;

void rwlock::lock_read(){
    
    lck_readers.lock();
    
    if (readers==0) {
        lck.lock();
    }
    
    readers++;
    
    lck_readers.unlock();
}

void rwlock::lock_write(){
    lck.lock();
}

void rwlock::unlock(){
    lck_readers.lock();
    if (readers>0) {
        readers--;
        if (readers==0) {
            lck.unlock();
        }
    }else{
        lck.unlock();
    }
    lck_readers.unlock();
}
