//
//  tuple_to_string.h
//  CppUtility
//
//  Created by 张琪 on 14/11/11.
//
//

#ifndef __CppUtility__tuple_to_string__
#define __CppUtility__tuple_to_string__

#include "CppUtility.common.h"

namespace util {
    template <typename Tuple,size_t Sz=tuple_size<Tuple>::value,size_t Cur=Sz-1>
    struct __tuple_runtime;
    
    template <typename Tuple,size_t Sz>
    struct __tuple_runtime<Tuple,Sz,0>;
    
    template <typename Tuple,size_t Sz,size_t Cur>
    struct __tuple_runtime{
        static void collect(const Tuple & tp, size_t index, stringstream & ss){
            if(index==Cur){
                ss<<get<Cur>(tp);
            }else if(Cur>0){
                __tuple_runtime<Tuple,Sz,Cur-1>::collect(tp,index,ss);
            }
        }
    };
    
    template <typename Tuple,size_t Sz>
    struct __tuple_runtime<Tuple,Sz,0>{
        static void collect(const Tuple & tp, size_t index, stringstream & ss){
            ss<<get<0>(tp);
        }
    };
    
    template<typename ...Tp,size_t sz = tuple_size<tuple<Tp...>>::value>
    string to_string(const tuple<Tp...> & tp, string div=","){
        stringstream ss;
        
        for (size_t i=0; i<sz; i++) {
            __tuple_runtime<tuple<Tp...>>::collect(tp,i,ss);

            if(i<sz-1) ss<<div;
        }
        
        return move(ss.str());
    }
    
    template<typename ...Tp>
    ostream & operator<<(ostream & s, const tuple<Tp...> & tp){
        s<<to_string(tp);
        return s;
    }
}

#endif /* defined(__CppUtility__tuple_to_string__) */
