//
//  Zip.h
//  CppUtility
//
//  Created by 张琪 on 13-10-29.
//
//

#ifndef __CppUtility__Zip__
#define __CppUtility__Zip__

#include "ByteArray.h"

namespace util {
    namespace Zip{
        shared_ptr<ByteArray> createCompressedData(const ByteArray *);
        shared_ptr<ByteArray> createUncompressedData(const ByteArray *);
    };
}

#endif /* defined(__CppUtility__Zip__) */
