//
//  times.h
//  CppUtility
//
//  Created by 张琪 on 14-9-5.
//
//

#ifndef __CppUtility__times__
#define __CppUtility__times__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    /**
     * in milliseconds. 1 second = 1000 milliseconds.
     */
    typedef unsigned long timestamp;

    /**
     * in milliseconds. 1 second = 1000 milliseconds.
     */
    typedef timestamp timeinterval;
    
    /**
     * current timestamp in milliseconds
     */
    inline timestamp now(){
        timeb tb;
        ftime(&tb);
        return tb.time*1000+tb.millitm;
    }
    
    struct update_time{
        timestamp current;
        timestamp changed;
    };
}

#endif /* defined(__CppUtility__times__) */
