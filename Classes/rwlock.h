//
//  rwlock.h
//  util
//
//  Created by 张琪 on 14-7-1.
//
//

#ifndef __util__rwlock__
#define __util__rwlock__

#include "spinlock.h"

namespace util {
    class rwlock{
        spinlock lck,lck_readers;
        unsigned int readers;
    public:
        void lock_read();
        void lock_write();
        void unlock();
    };    
}

#endif /* defined(__util__rwlock__) */
