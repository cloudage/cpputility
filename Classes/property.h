//
//  property.h
//  CppUtility
//
//  Created by 张 琪 on 15/2/11.
//
//

#ifndef __CppUtility__property__
#define __CppUtility__property__

#include "CppUtility.common.h"
#include "event.h"

namespace util {
    template <typename T>
    class property {
        T & _ref;
        
    public:
        inline property(T & ref):_ref(ref){}
        
        struct{
            event<> onChanged;
        }events;
        
        
        inline void set(const T & val){
            _ref = val;
            events.onChanged();
        }
        
        inline const T & get() const { return _ref; }
        
        inline property<T> & operator=(const T & val){
            set(val);
            return *this;
        }
        
        inline operator const T & () const{
            return _ref;
        }
        
        inline property<T> & operator+=(const T & val){
            _ref += val;
            events.onChanged();
            return *this;
        }
        
        inline property<T> & operator-=(const T & val){
            _ref -= val;
            events.onChanged();
            return *this;
        }
    };
}

#endif /* defined(__CppUtility__property__) */
