//
//  RCObject.h
//  CppUtility
//
//  Created by 张 琪 on 14/11/30.
//
//

#ifndef __CppUtility__RCObject__
#define __CppUtility__RCObject__

#include "CppUtility.common.h"
#include "spinlock.h"
#include "RTTIVisible.h"

namespace util{
    
    class RCObject;
    
    template<class CLS> CLS * RCRetain(CLS *);
    template<class CLS,typename ...TArgs> CLS * RCNew(TArgs...);
    inline void RCRelease(const RCObject *);
    inline bool RCExpired(const RCObject *);
    inline size_t RCRetainCount(const RCObject *);
    
    class RCObject{
        template<class CLS> friend CLS * RCRetain(CLS *);
        friend void RCRelease(const RCObject *);
        friend bool RCExpired(const RCObject *);
        friend size_t RCRetainCount(const RCObject *);
        
        mutable size_t _rc;
        mutable spinlock _lock;
        
        void retain() const;
        void release() const;
        
        size_t retain_count() const;
        
        static bool expired(const RCObject *);
        
    public:
        RTTI_VISIBLE_ROOT;
        
        RCObject();
        virtual ~RCObject();
    };
    
    template<class CLS> CLS * RCRetain(CLS * obj){
        if(obj!=nullptr) obj->retain();
        return obj;
    }
    
    inline void RCRelease(const RCObject * obj) {
        if(!RCExpired(obj)) obj->release();
    }
    
    inline bool RCExpired(const RCObject * obj) {
        return obj==nullptr || RCObject::expired(obj);
    }
    
    inline size_t RCRetainCount(const RCObject * obj){
        return RCExpired(obj) ? 0 : obj->retain_count();
    }
    
    template<class CLS> class strong{
        CLS * _obj = nullptr;
        
        static_assert(is_base_of<RCObject, CLS>::value,"CLS must be a subclass of RCObject");
    public:
        inline const CLS * get() const { return _obj; }
        inline CLS * get() { return _obj; }
        
        inline void set(CLS * obj){ RCRetain(obj); RCRelease(_obj); _obj = obj; }
        
        inline strong(CLS * obj) { _obj = RCRetain(obj); }
        inline strong(){ _obj = nullptr; }
        inline strong(strong<CLS> && ro){ _obj = RCRetain(ro._obj); }
        inline strong(const strong<CLS> & o){ _obj = RCRetain(o._obj); }
        inline ~strong(){ RCRelease(_obj); }
        
        inline const CLS * operator->() const { return get(); }
        inline CLS * operator->() { return get(); }
        inline operator const CLS * () const { return get(); }
        inline operator CLS * () { return get(); }
        inline strong<CLS> & operator=(CLS * obj) { set(obj); return *this;}
        inline strong<CLS> & operator=(const strong<CLS> & o){
            set(o._obj); return *this;
        }
        inline strong<CLS> & operator=(strong<CLS> && ro){ set(ro._obj); return *this;}
        inline bool operator==(CLS * obj) { return get()==obj; }
    };
    
    template<class CLS> class weak{
        CLS * _obj = nullptr;

        static_assert(is_base_of<RCObject, CLS>::value,"CLS must be a subclass of RCObject");
        
    public:
        inline weak(CLS * obj = nullptr ){ _obj = obj; }
        
        inline const CLS * get() const { return RCExpired(_obj) ? nullptr : _obj; }
        inline CLS * get() { return RCExpired(_obj) ? nullptr : _obj; }
        inline void set(CLS * obj) { _obj = obj; }
        
        inline const CLS * operator->() const { return get(); }
        inline CLS * operator->() { return get(); }
        inline operator const CLS * () const { return get(); }
        inline operator CLS * () { return get(); }
        inline void operator=(CLS * obj) { set(obj); }
        inline bool operator==(CLS * obj) { return get()==obj; }
    };
}

#endif /* defined(__CppUtility__RCObject__) */
