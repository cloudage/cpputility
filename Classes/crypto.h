//
//  crypto.h
//  util
//
//  Created by 张琪 on 14-4-24.
//
//

#ifndef __util__crypto__
#define __util__crypto__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    namespace digest{
        string md5(const string &);
        string md5(const void *, size_t);
    }
}

#endif /* defined(__util__crypto__) */
