//
//  astar.cpp
//  CppUtility
//
//  Created by 张 琪 on 14-9-3.
//  refrence "http://en.wikipedia.org/wiki/A*_search_algorithm"
//

#include "astar.h"


using namespace util;

astar_node low_f_node(const set<astar_node> & open, map<astar_node, uint> f_score){
    uint min_f = UINT_MAX;
    astar_node found = 0;
    
    for (auto node : open) {
        uint f = 0;
        auto iter_f = f_score.find(node);
        if (iter_f!=f_score.end()) {
            f = iter_f->second;
        }
        
        if (f<min_f) {
            min_f = f;
            found = node;
        }
    }
    
    return found;
}

list<astar_node> build_path(map<astar_node, astar_node> from, astar_node current){
    list<astar_node> path;
    
    path.push_back(current);
    
    auto iter = from.find(current);
    while (iter!=from.end()) {
        current = iter->second;
        
        from.erase(iter);
        
        path.push_front(current);
        
        iter = from.find(current);
    }
    
    return move(path);
}

list<astar_node> util::astar(astar_node start, astar_node end,
                               func_astar_neighbors neighbors,
                               func_astar_heuristic h,
                               func_astar_distance dist){
    
    map<astar_node,uint> g_score,h_score,f_score;
    g_score[start] = 0;
    h_score[start] = h(start,end);
    f_score[start] = g_score[start] + h_score[start];
    
    map<astar_node, astar_node> from;
    set<astar_node> open, close;
    
    open.insert(start);
    
    while (!open.empty()) {
        astar_node x = low_f_node(open, f_score);
        
        if (x==end) {
            auto path = build_path(from, end);
            if (!path.empty() && path.front()!=start) {
                path.clear();
            }
            return move(path);
        }
        
        open.erase(x);
        close.insert(x);
        
        for (auto y : neighbors(x)) {
            if (close.find(y)!=close.end()) continue;
            
            auto g = g_score[x] + dist(x, y);
            
            bool better;
            
            if (open.find(y)==open.end()) {
                open.insert(y);
                better = true;
            }else if(g < g_score[y]){
                better = true;
            }else{
                better = false;
            }
            
            if (better) {
                from[y] = x;
                g_score[y] = g;
                h_score[y] = h(y,end);
                f_score[y] = g_score[y] + h_score[y];
            }
        }
    }
    
    return move(list<astar_node>());
}