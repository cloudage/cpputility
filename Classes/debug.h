//
//  debug.h
//  CppUtility
//
//  Created by 张 琪 on 13-9-16.
//
//

#ifndef __CppUtility__debug__
#define __CppUtility__debug__

#include "CppUtility.common.h"

namespace util {
    string trace(int offset = 1);
    
    class error{
        string _what;
        string _stack;

    public:
        error(const string & what);
        inline const string & what() const { return _what; }
        inline const string & stack() const { return _stack; }
    };
    
    ostream & operator<<(ostream &,const error &);
}

#endif /* defined(__fortress__debug__) */
