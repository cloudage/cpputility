//
//  ByteArray.h
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#ifndef __CppUtility__ByteArray__
#define __CppUtility__ByteArray__

#include "CppUtility.common.h"
#include "RCObject.h"

namespace util {
    class ByteArray{
        void * bytes;
        size_t size;
        
    public:
        class writer{
        private:
            ByteArray * dest;
            size_t cur;
        public:
            inline writer(ByteArray * target){
                dest=target;
                cur = 0;
            }
            
            inline void writeBytes(const void * src, size_t sz){
                dest->size = cur+sz;
                dest->bytes = realloc(dest->bytes, dest->size);
                memcpy((unsigned char *)dest->bytes+cur, src, sz);
                cur += sz;
            }
            
            template<typename T> void writeValue(T val){
                writeBytes(&val, sizeof(T));
            }
        };
        class reader{
        private:
            const ByteArray * src;
            size_t cur;
        public:
            inline reader(const ByteArray * target){
                src = target;
                cur = 0;
            }
            
            inline void readBytes(void * dst, size_t sz){
                memcpy(dst, (unsigned char *)src->bytes+cur, sz);
                cur += sz;
            }
            
            template<typename T> void readValue( T & dst){
                readBytes(&dst, sizeof(T));
            }
        };
        
    public:
        inline void * getBytes() { return bytes; }
        inline const void * getBytes() const { return bytes; }
        
        inline size_t getSize() const { return size; }
        
        ByteArray(size_t sz = 0, const void * raw = 0, bool copy = true);
        ByteArray(const string &);
        ~ByteArray();
        
        ByteArray(const ByteArray * copy);
        
        inline writer makeWriter(){ return move(writer(this)); }
        inline reader makeReader() const{ return move(reader(this)); }
        
        string makeText() const;
    };
    
    template<typename T> ByteArray::writer & operator<<(ByteArray::writer & writer, T val){
        writer.writeValue(val);
        return writer;
    }
    
    template<typename T> ByteArray::reader & operator>>(ByteArray::reader & reader, T & val){
        reader.readValue(val);
        return reader;
    }
    
    ByteArray::writer & operator<<(ByteArray::writer & writer, const string & val);
    ByteArray::reader & operator>>(ByteArray::reader & reader, string & val);
    ByteArray::writer & operator<<(ByteArray::writer & writer, const wstring & val);
    ByteArray::reader & operator>>(ByteArray::reader & reader, wstring & val);
}

#endif /* defined(__fortress__bytearray__) */
