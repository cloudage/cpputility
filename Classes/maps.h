//
//  maps.h
//  CppUtility
//
//  Created by 张琪 on 14-9-5.
//
//

#ifndef __CppUtility__maps__
#define __CppUtility__maps__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    template<typename keyT,typename valT>
    set<keyT> all_key(map<keyT,valT> mp){
        set<keyT> result;
        for(auto item : mp) result.insert(item.first);
        return move(result);
    }
    
    template<typename keyT,typename valT>
    list<valT> all_value(map<keyT,valT> mp){
        list<valT> result;
        for(auto item : mp) result.insert(item.second);
        return move(result);
    }
}

#endif /* defined(__CppUtility__maps__) */
