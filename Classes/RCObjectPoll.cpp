//
//  ObjectPoll.cpp
//  CppUtility
//
//  Created by 张琪 on 15/11/17.
//
//

#include "RCObjectPoll.h"

using namespace util;

void RCObjectPoll::drain(){
    for (auto & i : _poll) {
        auto & lst = i.second;
        
        auto iter = lst.begin();
        while (iter!=lst.end()) {
            auto next_iter = next(iter);
            
            if (RCRetainCount(iter->get())==1) {
                lst.erase(iter);
            }
            
            iter = next_iter;
        }
    }
}