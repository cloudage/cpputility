//
//  dial.cpp
//  CppUtility
//
//  Created by 张琪 on 14/12/2.
//
//

#include "dial.h"

using namespace util;

void dial::adjust(){
    if (_current>=_max) {
        auto dr = div(_current-_min, _max-_min);
        int carry = dr.quot;
        _current = dr.rem + _min;
        
        
        if (_migration!=nullptr) {
            _migration->add(carry);
        }
    }else if (_current<_min) {
        auto dr = div(_current-_min, _max-_min);
        int carry = dr.quot + 1;
        _current = _max+dr.rem;
        
        
        if (_migration!=nullptr) {
            _migration->add(-carry);
        }
    }
}

dial::dial(int pmin, int pmax, dial * pmigration){
    pmax+=1;
    _min = min(pmin,pmax);
    _max = max(pmin, pmax);
    _current = _min;
    _migration = pmigration;
}

int dial::get() const { return _current; }

void dial::add(int val){
    _current += val;
    adjust();
}

void dial::set(int val){
    _current = val;
    adjust();
}