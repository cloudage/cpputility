//
//  lazy.h
//  CppUtility
//
//  Created by 张 琪 on 14-10-8.
//
//

#ifndef __CppUtility__lazy__
#define __CppUtility__lazy__

#include "CppUtility.common.h"
#include "rwlock.h"
#include "event.h"

namespace util {
    template<typename T> class lazy{
        mutable T _val;
        mutable function<T()> _fload;
        mutable bool _invalidated = true;

    public:
        typedef typename add_lvalue_reference<T>::type type_ref;
        typedef typename add_const<type_ref>::type type_cref;
        
        lazy(function<T()> fload):_fload(fload){}
        
        type_cref get() const{
            if (_invalidated) {
                _val = _fload();
                _invalidated = false;
            }
            return _val;
        }
        
        operator type_cref () const{ return get(); }
        
        void invalidate(){
            _invalidated = true;
        }
    };
}

#endif /* defined(__CppUtility__lazy__) */
