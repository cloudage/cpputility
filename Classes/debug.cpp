//
//  debug.cpp
//  CrossPlatformGraphics
//
//  Created by 张 琪 on 13-9-16.
//
//

#include "debug.h"
#include <execinfo.h>
#include <signal.h>
#include "strings.h"
#include "times.h"

using namespace util;

string util::trace(int offset){
    void * buf[0xff];
    int sz = backtrace(buf, 0xff);
    char ** smpls = backtrace_symbols(buf, sz);
    if (smpls) {
        stringstream ss;
        ss<<"stack trace "<<sz-offset<<" symbols:"<<endl;
        for (int i=offset; i<sz; i++) {
            ss<<smpls[i]<<endl;
        }
        free(smpls);
        return ss.str();
    }else{
        return "";
    }
}

error::error(const string & str):_what(str),_stack(trace(0)){

}

ostream & util::operator<<(ostream & s, const error & e){
    return s<<timestamp()<<"[!ERROR!] "<<e.what()<<endl<<e.stack()<<endl;
}

namespace util {
    class SignalTrace{
        static void abortHandler(int signum)
        {
            const char* name = NULL;
            switch( signum )
            {
                case SIGABRT: name = "SIGABRT";  break;
                case SIGSEGV: name = "SIGSEGV";  break;
                case SIGBUS:  name = "SIGBUS";   break;
                case SIGILL:  name = "SIGILL";   break;
                case SIGFPE:  name = "SIGFPE";   break;
            }
            
            cerr<<timestamp()<<"[!ABORT!] "<<(name==0?to_string(signum):name)<<endl<<trace()<<endl;
            
            signal(signum, SIG_DFL);
        }
        
    public:
        SignalTrace(){
            signal( SIGABRT, abortHandler );
            signal( SIGSEGV, abortHandler );
            signal( SIGILL,  abortHandler );
            signal( SIGFPE,  abortHandler );
        }
    };
}

static SignalTrace globalSignalTrace;