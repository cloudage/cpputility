//
//  run.h
//  CppUtility
//
//  Created by 张琪 on 14-7-28.
//
//

#ifndef __CppUtility__run__
#define __CppUtility__run__

#include "CppUtility.common.h"
#include "times.h"

namespace util {
    
    enum run_queue{
        run_queue_main=0,
        run_queue_async,
        run_queue_background,
    };
    
    void run(function<void ()>, run_queue queue, timeinterval later);
    
    class run_group{
        struct Internal;
        Internal * _internal;

    public:
        run_group();
        ~run_group();
        
        void run(function<void ()>, run_queue queue, timeinterval later);
        
        void wait();
    };
}

#endif /* defined(__CppUtility__run__) */
