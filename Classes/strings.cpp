//
//  strings.cpp
//  CppUtility
//
//  Created by 张琪 on 14-9-5.
//
//

#include "strings.h"

using namespace util;

bool util::compare_string_head(const string &str, const string &head){
    auto istr = str.begin();
    auto ihead = head.begin();
    
    while (ihead!=head.end()) {
        if (istr==str.end() || *istr!=*ihead) {
            return false;
        }
        
        ihead++;
        istr++;
    }
    
    return true;
}

bool util::compare_string_tail(const string &str, const string &tail){
    auto istr = str.rbegin();
    auto itail = tail.rbegin();
    
    while (itail!=tail.rend()) {
        if (istr==str.rend() || *istr!=*itail) {
            return false;
        }
        
        istr++;
        itail++;
    }
    
    return true;
}

vector<string> util::explode(const string & v, char c){
    vector<string> ret;
    string found;
    
    for(size_t i = 0;i<v.size();i++){
        char ic = v.at(i);
        if (ic==c && found.length()>0) {
            ret.push_back(found);
            found.clear();
        }else if(ic!=c){
            found += ic;
        }
    }
    
    if (found.length()>0) {
        ret.push_back(found);
    }
    
    return ret;
}