//
//  RCObject.cpp
//  CppUtility
//
//  Created by 张 琪 on 14/11/30.
//
//

#include "RCObject.h"

using namespace util;

set<const RCObject *> _inst;
spinlock _instlock;

RCObject::RCObject(){
    _rc=0;
    _instlock.lock();
    _inst.insert(this);
    _instlock.unlock();
}

bool RCObject::expired(const RCObject * obj){
    _instlock.lock();
    bool ret = _inst.find(obj) == _inst.end();
    _instlock.unlock();
    return ret;
}

RCObject::~RCObject(){
    _instlock.lock();
    _inst.erase(this);
    _instlock.unlock();
}

void RCObject::retain() const{
    _lock.lock();
    _rc++;
    _lock.unlock();
}

void RCObject::release() const{
    
    if (instanceOf<RCObject>(this)) {
        
    }
    _lock.lock();
    _rc--;
    if(_rc==0){
        _lock.unlock();
        delete this;
    }else{
        _lock.unlock();
    }
}

size_t RCObject::retain_count() const{
    _lock.lock();
    auto ret = _rc;
    _lock.unlock();
    return ret;
}

RTTI_IMPL_BEGIN(RCObject)
RTTI_IMPL_END