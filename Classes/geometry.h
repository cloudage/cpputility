//
//  geometry.h
//  CppUtility
//
//  Created by 张琪 on 15/7/22.
//
//

#ifndef __CppUtility__geometry__
#define __CppUtility__geometry__

#include "CppUtility.common.h"

namespace util {
    
    template<typename T> struct point{
        T x,y;
        
        inline point<T> operator - (const point<T> & p) const{
            return { x-p.x, y-p.y };
        }
        
        inline point<T> operator + (const point<T> & p) const{
            return { x+p.x, y+p.y };
        }
        
        inline point<T> operator * (float s) const{
            return { x*s, y*s};
        }
        
        inline T length() const {
            return sqrt(x*x+y*y);
        }
    };
    
    template<typename T> struct size{
        T width,height;
    };
    
    template<typename Tpoint,typename Tsize> struct rect{
        
        union{
            struct{
                point<Tpoint> origin;
                size<Tsize> size;
            };
            
            Tpoint x,y;
            Tsize width,height;
        };
    };
    
    typedef point<float> fpoint;
    typedef size<float> fsize;
    typedef rect<float,float> frect;
    
    typedef point<int> ipoint;
    typedef size<int> isize;
    typedef rect<int, int> irect;
    
    typedef size<unsigned int> usize;
    typedef rect<int, unsigned int> iurect;
}

#endif /* defined(__CppUtility__geometry__) */
