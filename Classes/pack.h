//
//  pack.h
//  CppUtility
//
//  Created by 张 琪 on 14-9-6.
//
//

#ifndef __CppUtility__pack__
#define __CppUtility__pack__

#include "CppUtility.common.h"

namespace util{
    using namespace std;
    
    class pack{
        void * _bytes;
        size_t _size;

        struct{
            string name;
            size_t hash;
        }_type;
        
        void set(const void *, size_t);
        void set(const type_info & type);
        bool cmp(const void *, size_t) const;
    
    public:
        pack();
        pack(const void *,size_t);
        ~pack();
        
        inline const void * bytes() const { return _bytes; }
        inline size_t size() const { return _size; }
        inline bool empty() const { return _size==0; }
        
        pack(const pack &);
        void operator=(const pack &);
        
        bool operator==(const pack &) const;
        
        template<typename T> pack(const T & val){
            set(&val, sizeof(T));
            set(typeid(T));
        }

        template<typename T> T unpack() const{
            static T none;
            if (_type.hash!=typeid(T).hash_code()) return none;

            T val;
            memcpy(&val, _bytes, _size);
            return val;
        }
        
        template<typename T> void operator=(const T & val){
            set(&val,sizeof(T));
            set(typeid(T));
        }
        
        template<typename T> bool operator==(const T & val) const{
            if (_type.hash!=typeid(T).hash_code()) return false;
            return cmp(&val, sizeof(T));
        }
    };
}

#endif /* defined(__CppUtility__pack__) */
