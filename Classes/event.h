//
//  event.h
//  CppUtility
//
//  Created by 张琪 on 13-9-13.
//
//

#ifndef __CppUtility__event__
#define __CppUtility__event__

#include "CppUtility.common.h"
#include "function_signature.h"
#include "strings.h"

namespace util {
    
    template <class ...Targs>
    class event {
        
        mutable map<void*,list<function<void(Targs...)>>> _callbacks;
        
    public:
        void clear() const{
            _callbacks.clear();
        }
        
        void add(void* tag, function<void(Targs...)> func){
            _callbacks[tag].push_back(func);
        }
        
        void remove(void* tag){
            _callbacks.erase(tag);
        }
        
        void invoke(Targs... args) const{
            auto callbacks = _callbacks;
            for(auto & calllst : callbacks){
                for(auto & call : calllst.second)
                    call(args...);
            }
        }
        
        void operator()(Targs... args) const{
            invoke(args...);
        }
    };
}

#endif /* defined(__CppUtility__event__) */
