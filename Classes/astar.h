//
//  astar.h
//  CppUtility
//
//  Created by 张 琪 on 14-9-3.
//
//

#ifndef __CppUtility__astar__
#define __CppUtility__astar__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    typedef unsigned long astar_node;
    
    typedef function<list<astar_node>(astar_node)> func_astar_neighbors;
    typedef function<uint(astar_node,astar_node)> func_astar_heuristic;
    typedef function<uint(astar_node,astar_node)> func_astar_distance;
    
    list<astar_node> astar(astar_node start,astar_node end,
                           func_astar_neighbors,
                           func_astar_heuristic,
                           func_astar_distance);
}

#endif /* defined(__CppUtility__astar__) */
