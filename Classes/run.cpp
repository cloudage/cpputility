//
//  run.cpp
//  CppUtility
//
//  Created by 张琪 on 14-7-28.
//
//

#include "run.h"
#include "debug.h"

using namespace util;

#if __APPLE__
#include <dispatch/dispatch.h>
// use GCD


dispatch_queue_t rq2dq(util::run_queue q){
    switch (q) {
        case run_queue_main:
            return dispatch_get_main_queue();
            
        case run_queue_async:
            return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            
        case run_queue_background:
            return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
            
        default:
            throw error("unknown run queue");
    }
}

void util::run(function<void ()> func, run_queue queue, timeinterval later){
    dispatch_queue_t dpq = rq2dq(queue);
    
    if (later>0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(later * NSEC_PER_MSEC)),
                       dpq,
                       ^{ func(); });
    }else{
        dispatch_async(dpq, ^{ func(); });
    }
}

struct util::run_group::Internal{
    dispatch_group_t group;
};

util::run_group::run_group(){
    _internal = new Internal;
    _internal->group = dispatch_group_create();
}

util::run_group::~run_group(){
    wait();
    delete _internal;
}

void util::run_group::run(function<void ()> func, run_queue q, timeinterval later){
    dispatch_group_async(_internal->group, rq2dq(q), ^{
        if (later>0) {
            this_thread::sleep_for(chrono::milliseconds(later));
        }

        func();
    });
}

void util::run_group::wait(){
    dispatch_group_wait(_internal->group, DISPATCH_TIME_FOREVER);
}

#else
#error task dispatching not implemented
#endif