//
//  XML.cpp
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#include "XML.h"
#include "StorageManager.h"
#include "ByteArray.h"
#include "debug.h"
#include "strings.h"
#include <libxml/parser.h>

using namespace util;

namespace util {
    class libxmlCallbacks {
        
    private:
        int resultCode = -1;
        
        stack<data_node> elementStack;
        
        data_node resultElement;
        
        string errmsg;
        
        static void startElementSAXFunc(void *ctx, const xmlChar *name, const xmlChar **atts){
            libxmlCallbacks * cb = (libxmlCallbacks*)ctx;
            data_node e;
            e.name = (const char *)name;
            
            size_t idx = 0;
            if (atts!=0) {
                while (atts[idx]!=0) {
                    const char * key = (const char *)atts[idx];
                    const char * val = (const char *)atts[idx+1];
                    e.attributes[key]=val;
                    idx+=2;
                }
            }
            
            cb->elementStack.push(e);
        }
        
        static void endElementSAXFunc (void *ctx, const xmlChar *name){
            libxmlCallbacks * cb = (libxmlCallbacks*)ctx;
            
            cb->resultElement = cb->elementStack.top();
            cb->elementStack.pop();
            
            if (!cb->elementStack.empty()) {
                data_node & top = cb->elementStack.top();
                top.children.push_back(cb->resultElement);
            }
        }
        
        static void warningSAXFunc (void *ctx, const char *msg, ...){
            va_list ap;
            va_start(ap, msg);
            cerr<<"warning on parsing XML:"<<format(msg, ap)<<endl;
            va_end(ap);
        }
        
        static void errorSAXFunc (void *ctx, const char *msg, ...){
            libxmlCallbacks * cb = (libxmlCallbacks*)ctx;
            
            va_list ap;
            va_start(ap, msg);
            cb->errmsg = format(msg, ap);
            va_end(ap);
        }
        
        static void fatalErrorSAXFunc (void *ctx, const char *msg, ...){
            libxmlCallbacks * cb = (libxmlCallbacks*)ctx;
            
            va_list ap;
            va_start(ap, msg);
            cb->errmsg = format(msg, ap);
            va_end(ap);
        }
        
//        static void cdataSAXFunc (void *ctx, const xmlChar *value, int len){
//            libxmlCallbacks * cb = (libxmlCallbacks*)ctx;
//            
//            string cdata = string((const char *)value,len);
//            cb->elementStack.top().cdata.push_back(cdata);
//        }
        
        static xmlSAXHandlerPtr getSaxHandler() {
            static xmlSAXHandlerPtr inst = 0;
            spinlock_atomic;
            
            if (inst==0) {
                inst = new xmlSAXHandler;
                memset(inst, 0, sizeof(xmlSAXHandler));
                inst->startElement = &libxmlCallbacks::startElementSAXFunc;
                inst->endElement = &libxmlCallbacks::endElementSAXFunc;
//                inst->cdataBlock = &libxmlCallbacks::cdataSAXFunc;
                inst->warning = &libxmlCallbacks::warningSAXFunc;
                inst->error = &libxmlCallbacks::errorSAXFunc;
                inst->fatalError = &libxmlCallbacks::fatalErrorSAXFunc;
            }
            
            return inst;
        }
        
    public:
        int getResultCode() const {
            return resultCode;
        }
        
        const string & getErrorMessage() const {
            return errmsg;
        }
        
        data_node getResult() const {
            return resultElement;
        }
        
        void parse(string content){
            resultCode = xmlSAXUserParseMemory(getSaxHandler(), this, content.c_str(), (int)content.size());
        }
    };
}

data_node util::parseXML(string src, bool * out_success, string * out_errmsg){
    libxmlCallbacks cb;
    cb.parse(src);
    
    string resultMsg = cb.getErrorMessage();
    
    if (resultMsg.empty()) {
        if (out_success!=0) *out_success = true;
        if (out_errmsg!=0) *out_errmsg = "";
    }else{
        if (out_success!=0) *out_success = false;
        if (out_errmsg!=0) *out_errmsg = resultMsg;
        string errstr = "error on parsing XML:"+resultMsg;
        cerr<<errstr<<endl;
    }
    
    return cb.getResult();
}

void data_node_to_xml_string(const data_node & e, stringstream & ss, string indent=""){
    ss<<indent<<'<'<<e.name;
    
    if (!e.attributes.empty()) {
        for (auto attr : e.attributes) {
            ss<<' '<<attr.first<<'='<<'"'<<attr.second.asString()<<'"';
        }
    }
    
    if (e.children.empty()) {
        ss<<"/>"<<endl;
    }else{
        ss<<'>'<<endl;

        string subidnt = indent + "  ";
        
        for (const data_node & sub : e.children) {
            data_node_to_xml_string(sub, ss, subidnt);
        }
        
        ss<<indent<<"</"<<e.name<<'>'<<endl;
    }
}

string util::printXML(const data_node & node){
    stringstream ss;
    data_node_to_xml_string(node, ss);
    return ss.str();
}