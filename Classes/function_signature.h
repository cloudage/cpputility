//
//  function_signature.h
//  CppUtility
//
//  Created by 张琪 on 14/11/11.
//
//

#ifndef __CppUtility__function_signature__
#define __CppUtility__function_signature__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    template<typename TRet,class CLS,typename ...TArgs>
    string signature(TRet(CLS::*method)(TArgs...)const){
        union{
            TRet(CLS::*_method)(TArgs...)const;
            unsigned long long _address;
        };
        
        _address = 0;
        _method = method;
        
        stringstream ss;
        ss<<typeid(decltype(method)).name()<<"@"<<_address;
        return move(ss.str());
    }
    
    template<typename TRet,class CLS,typename ...TArgs>
    string signature(TRet(CLS::*method)(TArgs...)){
        union{
            TRet(CLS::*_method)(TArgs...);
            unsigned long long _address;
        };
        
        _address = 0;
        _method = method;
        
        stringstream ss;
        ss<<typeid(decltype(method)).name()<<"@"<<_address;
        return move(ss.str());
    }
    
    template<typename TRet,typename ...TArgs>
    string signature(TRet(*func)(TArgs...)){
        union{
            TRet(*_func)(TArgs...);
            unsigned long long _address;
        };
        
        _address = 0;
        _func = func;
        
        stringstream ss;
        ss<<typeid(decltype(func)).name()<<"@"<<_address;
        return move(ss.str());
    }
}

#endif /* defined(__CppUtility__function_signature__) */
