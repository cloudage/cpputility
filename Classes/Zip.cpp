//
//  Zip.cpp
//  CppUtility
//
//  Created by 张琪 on 13-10-29.
//
//

#include "Zip.h"
#include <zlib.h>

using namespace util;

shared_ptr<ByteArray> Zip::createCompressedData(const ByteArray * source){
    uLong bound = compressBound(source->getSize());
    void * buf = malloc(bound);
    int ret = ::compress((Bytef *)buf, &bound, (const Bytef *)source->getBytes(), source->getSize());
    
    if (ret==Z_OK) {
        size_t datalen = sizeof(size_t) + bound;
        ByteArray * ret = new ByteArray(datalen);
        
        ByteArray::writer wtr = ret->makeWriter();
        wtr.writeValue(source->getSize());
        wtr.writeBytes(buf, bound);
        
        free(buf);
        
        return shared_ptr<ByteArray>(ret);
    }else{
        free(buf);
        return nullptr;
    }
}

shared_ptr<ByteArray> Zip::createUncompressedData(const ByteArray * zipped){
    if (zipped==nullptr) return nullptr;
    
    ByteArray::reader rdr = zipped->makeReader();
    size_t srcsz;
    rdr.readValue(srcsz);
    size_t zipsz = zipped->getSize() - sizeof(size_t);
    void * zipbuf = malloc(zipsz);
    rdr.readBytes(zipbuf, zipsz);
    void * unzipbuf = malloc(srcsz);
    int ret = ::uncompress((Bytef*)unzipbuf, &srcsz, (const Bytef*)zipbuf, zipsz);
    free(zipbuf);
    
    if (ret==Z_OK) {
        //ignore the potential leak warning of xcode analyze here
        //the ByteArray object will take ownership of bytes created above
        return make_shared<ByteArray>(srcsz,unzipbuf,false);
    }else{
        free(unzipbuf);
        return nullptr;
    }
}