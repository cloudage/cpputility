//
//  path.h
//  CppUtility
//
//  Created by 张琪 on 14-5-27.
//
//

#ifndef __CppUtility__path__
#define __CppUtility__path__

#include "CppUtility.common.h"
#include "value.h"

namespace util {
    class path{
        list<string> segments;
        char divider;
    public:
        path(string root="",char div='/');
        
        path & append(const value & sec);
        
        string str() const;
        
        path & operator<<(const value & sec);
        
        operator string();
    };
}

#endif /* defined(__CppUtility__path__) */
