//
//  CocoaStorageManager.cpp
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#include "CocoaStorageManager.h"

#include <Foundation/Foundation.h>
#include "debug.h"

using namespace util;

namespace util {
    class CocoaFileSystemStorage : public Storage{
        string dir;
        
        static void ensurePath(string path){
            NSString * nspath = [NSString stringWithUTF8String:path.c_str()];
            NSFileManager * fm = [NSFileManager defaultManager];
            BOOL isDir = NO;
            if ([fm fileExistsAtPath:nspath isDirectory:&isDir] && isDir) {
                return;
            }
            
            NSError * err = nil;
            [fm createDirectoryAtPath:nspath
          withIntermediateDirectories:YES
                           attributes:nil
                                error:&err];
            if (err!=nil) {
                NSLog(@"failed to ensure directory '%@'",nspath);
                NSLog(@"with error '%@'",err.localizedDescription);
                throw error(err.localizedDescription.UTF8String);
            }
        }
        
    public:
        CocoaFileSystemStorage(const string & path){
            @autoreleasepool {
                dir = path;
                if (dir.back()!='/') dir += '/';
                
                ensurePath(dir);
            }
        }
        
        virtual shared_ptr<ByteArray> read(const string & path) const override{
            spinlock_atomic;
            @autoreleasepool {
                string fullpath = dir + path;
                NSString * nspath = [NSString stringWithUTF8String:fullpath.c_str()];
                NSData * data = [NSData dataWithContentsOfFile:nspath];
                if (data==nil) {
                    return 0;
                }else{
                    const void * bytes = data.bytes;
                    size_t sz = data.length;
                    return make_shared<ByteArray>(sz,bytes);
                }
            }
        }
        
        virtual void write(const string & path, const ByteArray * data) override{
            spinlock_atomic;
            @autoreleasepool {
                string fullpath = dir + path;
                NSString * nspath = [NSString stringWithUTF8String:fullpath.c_str()];
                ensurePath([nspath stringByDeletingLastPathComponent].UTF8String);
                NSData * nsdata = [NSData dataWithBytesNoCopy:(void*)data->getBytes()
                                                       length:data->getSize()
                                                 freeWhenDone:NO];
                BOOL writen = [nsdata writeToFile:nspath atomically:NO];
                if (!writen) {
                    NSLog(@"write %@ failed",nspath);
                }
            }
        }
        
        virtual vector<string> list(const string & path) const override{
            spinlock_atomic;
            @autoreleasepool {
                string fullpath = dir + path;
                NSString * nspath = [NSString stringWithUTF8String:fullpath.c_str()];
                NSFileManager * fm = [NSFileManager defaultManager];
                NSArray * subpaths = [fm subpathsAtPath:nspath];
                vector<string> lst;
                for (NSString * sp in subpaths) {
                    lst.push_back(sp.UTF8String);
                }
                return lst;
            }
        }
    };
}

shared_ptr<Storage> CocoaStorageManager::get(string name){
    spinlock_atomic;
    @autoreleasepool {
        typedef map<string, weak_ptr<Storage>> StorageMap;
        
        static StorageMap storagis;
        
        StorageMap::iterator iter = storagis.find(name);
        
        if (iter==storagis.end()) {
            NSString * bundleName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
            
            NSArray * docDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            NSString * docDir = [docDirs objectAtIndex:0];
            
            NSString * path = [docDir stringByAppendingPathComponent:bundleName];
            
            NSString * dirname = [NSString stringWithUTF8String:name.c_str()];
            
            path = [path stringByAppendingPathComponent:dirname];
            
            shared_ptr<Storage> inst(new CocoaFileSystemStorage(path.UTF8String));
            
            storagis[name]=inst;
            
            return inst;
        }
        
        return iter->second.lock();
    }
}

shared_ptr<Storage> CocoaStorageManager::getSystem(){
    spinlock_atomic;
    
    static shared_ptr<Storage> inst;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString * respath = [[NSBundle mainBundle] resourcePath];
        CocoaFileSystemStorage * fsstg = new CocoaFileSystemStorage(respath.UTF8String);
        inst = shared_ptr<Storage>(fsstg);
    });

    return inst;
}

shared_ptr<Storage> CocoaStorageManager::getCache(){
    spinlock_atomic;
    
    @autoreleasepool {
        static shared_ptr<Storage> inst;
        
        if (inst==0) {
            NSString * bundleName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
            
            NSArray * cacheDirs = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString * cacheDir = [[cacheDirs objectAtIndex:0] stringByAppendingPathComponent:bundleName];
            
            NSString * path = [cacheDir stringByAppendingPathComponent:bundleName];
            
            inst = make_shared<CocoaFileSystemStorage>(path.UTF8String);
        }
        
        return inst;
    }
}