//
//  CocoaStorageManager.h
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#ifndef __CppUtility__CocoaStorageManager__
#define __CppUtility__CocoaStorageManager__

#include "StorageManager.h"

namespace util {
    class CocoaStorageManager:public StorageManager{
    public:        
        virtual shared_ptr<Storage> getCache() override;
        virtual shared_ptr<Storage> get(string name) override;
        virtual shared_ptr<Storage> getSystem() override;
    };
}

#endif /* defined(__CppUtility__CocoaStorageManager__) */
