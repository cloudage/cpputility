//
//  data_node.h
//  CppUtility
//
//  Created by 张 琪 on 14-3-16.
//
//

#ifndef __CppUtility__data_node__
#define __CppUtility__data_node__

#include "CppUtility.common.h"
#include "value.h"

namespace util {
    struct data_attributes : map<string, value>{
        string opt_string(const string & name, const string & valDefault=null_str) const;
        int opt_int(const string & name, int valDefault=0) const;
        long opt_long(const string & name, long valDefault=0) const;
        unsigned int opt_uint(const string & name, unsigned int valDefault=0) const;
        unsigned long opt_ulong(const string & name,unsigned long valDefault=0) const;
        char opt_char(const string & name, char valDefault=0) const;
        double opt_double(const string & name, double valDefault=0) const;
        float opt_float(const string & name, float valDefault=0) const ;
        bool opt_bool(const string & name,bool valDefault=false) const;
    };
    
    struct data_node{
        string name;
        data_attributes attributes;
        vector<data_node> children;
        
        data_node() = default;
        inline data_node(const string & valName):name(valName){}
        inline data_node(const char * valName):name(valName){}
        
        const data_node & findFirstChildWithName(const string &) const;
        
        bool empty() const;
    };
}

#endif /* defined(__CppUtility__data_node__) */
