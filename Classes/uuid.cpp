//
//  uuid.cpp
//  util
//
//  Created by 张琪 on 13-11-12.
//
//

#include "uuid.h"
#include <inttypes.h>

using namespace std;
using namespace util;

const uuid uuid::zero(false);

uuid::uuid():uuid(true){}

uuid::uuid(const uuid & cp){
    memcpy(bytes, cp.bytes, sizeof(unsigned char)*16);
}

void uuid::operator=(const uuid &cp){
    memcpy(bytes, cp.bytes, sizeof(unsigned char)*16);
}

uuid::uuid(bool should_init){
    if (should_init) {
        init();
    }else{
        memset(bytes, 0, sizeof(unsigned char)*16);
    }
}

#if __APPLE__

#include <CoreFoundation/CoreFoundation.h>

void uuid::init(){
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    CFUUIDBytes cfbytes = CFUUIDGetUUIDBytes(uuidRef);
    memcpy(bytes, &cfbytes, sizeof(unsigned char)*16);
    CFRelease(uuidRef);
}

uuid::uuid(const char * str){
    CFStringRef uuidstr = CFStringCreateWithCString(kCFAllocatorDefault, str, kCFStringEncodingUTF8);
    CFUUIDRef uuidRef = CFUUIDCreateFromString(kCFAllocatorDefault, uuidstr);
    CFUUIDBytes cfbytes = CFUUIDGetUUIDBytes(uuidRef);
    memcpy(bytes, &cfbytes, sizeof(unsigned char)*16);
    CFRelease(uuidRef);
    CFRelease(uuidstr);
}

string uuid::str() const{
    CFUUIDRef uuidref = CFUUIDCreateFromUUIDBytes(kCFAllocatorDefault, *(CFUUIDBytes*)(bytes));
    CFStringRef uuidstr = CFUUIDCreateString(kCFAllocatorDefault, uuidref);
    string str = CFStringGetCStringPtr(uuidstr, kCFStringEncodingUTF8);
    CFRelease(uuidstr);
    CFRelease(uuidref);
    
    return str;
}

#else
#error uuid not implemented on this platform yet

#endif

bool uuid::operator<(const uuid &other) const{
    if (high<other.high) {
        return true;
    }else if (high==other.high){
        return low < other.low;
    }else{
        return false;
    }
}

bool uuid::operator==(const uuid &other) const{
    return high==other.high && low==other.low;
}