//
//  ObjectPoll.hpp
//  CppUtility
//
//  Created by 张琪 on 15/11/17.
//
//

#ifndef ObjectPoll_hpp
#define ObjectPoll_hpp

#include "RCObject.h"

namespace util {
    class RCObjectPoll{
        map<const char *, list<strong<RCObject>>> _poll;
        
    public:
        void drain();
        
        template<class T,typename ...TArgs>
        T * get(TArgs... args){

            auto tid = typeid(T).name();
            auto & lst = _poll[tid];
            
            for(RCObject * rcobj : lst){
                if (RCRetainCount(rcobj)==1) {
                    
                    T * obj = (T*)rcobj;
                    
                    obj->~T();
                    new (obj) T(args...);
                    
                    return obj;
                }
            }
            
            T * obj = new T(args...);
            lst.push_back(obj);
            return obj;
        }
    };
}

#endif /* ObjectPoll_hpp */
