//
//  data_node.cpp
//  CppUtility
//
//  Created by 张 琪 on 14-3-16.
//
//

#include "data_node.h"

using namespace util;

string data_attributes::opt_string(const string & name, const string & valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asString();
    }else{
        return valDefault;
    }
}

int data_attributes::opt_int(const string & name, int valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asInt();
    }else{
        return valDefault;
    }
}

long data_attributes::opt_long(const string &name, long valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asLong();
    }else{
        return valDefault;
    }
}

unsigned int data_attributes::opt_uint(const string &name, unsigned int valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asUInt();
    }else{
        return valDefault;
    }
}

unsigned long data_attributes::opt_ulong(const string &name, unsigned long valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asULong();
    }else{
        return valDefault;
    }
}

float data_attributes::opt_float(const string & name, float valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asFloat();
    }else{
        return valDefault;
    }
}

double data_attributes::opt_double(const string &name, double valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asDouble();
    }else{
        return valDefault;
    }
}

char data_attributes::opt_char(const string &name, char valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asChar();
    }else{
        return valDefault;
    }
}

bool data_attributes::opt_bool(const string & name,bool valDefault) const{
    auto iter = find(name);
    if (iter!=end()) {
        return iter->second.asBool();
    }else{
        return valDefault;
    }
}

bool data_node::empty() const{
    return attributes.empty() && children.empty();
}

const data_node & data_node::findFirstChildWithName(const string & name) const{
    static data_node empty;
    
    for (const data_node & node : children) {
        if (node.name == name) {
            return node;
        }
    }
    
    return empty;
}