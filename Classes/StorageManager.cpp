//
//  StorageManager.cpp
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#include "StorageManager.h"
#include "strings.h"

using namespace util;

vector<string> MemoryStorage::list(const string &path) const{
    vector<string> lst;
    
    bool wantAll = path==".";
    
    for (auto item : _map) {
        if (!wantAll && !compare_string_head(item.first, path)) {
            continue;
        }
        lst.push_back(item.first);
    }
    
    return move(lst);
}

shared_ptr<ByteArray> MemoryStorage::read(const string &path) const{
    spinlock_guard(_maplock);
    
    auto iter = _map.find(path);
    if (iter==_map.end()) {
        return 0;
    }else{
        return iter->second;
    }
}

void MemoryStorage::write(const string &path, const ByteArray * data){
    spinlock_guard(_maplock);
    
    if (data==0) {
        _map.erase(path);
    }else{
        _map[path]=make_shared<ByteArray>(data);
    }
}

shared_ptr<Storage> StorageManager::getMemory(string name){
    spinlock_atomic;
    
    return _memories[name];
}

#ifdef __APPLE__
#include "CocoaStorageManager.h"
StorageManager * StorageManager::get(){
    static atomic<StorageManager *> inst;

    if (inst==nullptr) {
        inst = new CocoaStorageManager;
    }
    
    return inst;
}
#else
#error StorageManager not implemented on this platform
#endif