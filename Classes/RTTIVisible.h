//
//  RTTIVisible.h
//  CppUtility
//
//  Created by 张琪 on 15/2/25.
//
//

#ifndef __CppUtility__RTTIVisible__
#define __CppUtility__RTTIVisible__

#include "CppUtility.common.h"

#define RTTI_VISIBLE \
static const char *Type; \
virtual bool is_a(const char *) const override; \
virtual const char * instance_type() const override { return Type; }

#define RTTI_VISIBLE_ROOT \
static const char *Type; \
virtual bool is_a(const char *) const; \
virtual const char * instance_type() const { return Type; } \
template<class CLS> bool is_a() const{ return is_a(CLS::Type); } \
template<class CLS> CLS * as(){ return is_a<CLS>() ? (CLS*)this : nullptr; } \
template<class CLS> const CLS * as() const { return is_a<CLS>() ? (const CLS *)this : nullptr; }

#define RTTI_IMPL_BEGIN(cls) \
const char * cls::Type = #cls; \
bool cls::is_a(const char * type) const \
{ if(type==Type) return true; \

#define RTTI_IMPL_CHECKBASE(cls) if(cls::is_a(type)) return true;

#define RTTI_IMPL_END return false; }

#define RTTI_IMPL_ROOT(cls) \
RTTI_IMPL_BEGIN(cls) \
RTTI_IMPL_END

#define RTTI_IMPL(cls,base) \
RTTI_IMPL_BEGIN(cls) \
RTTI_IMPL_CHECKBASE(base) \
RTTI_IMPL_END

namespace util {
    template<class Base,class T>
    bool instanceOf(T* instance){
        return instance != nullptr && instance->is_a(Base::Type);
    }
}

#endif /* defined(__CppUtility__RTTIVisible__) */
