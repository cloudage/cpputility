//
//  crypto.cpp
//  util
//
//  Created by 张琪 on 14-4-24.
//
//

#include "crypto.h"
#include <CommonCrypto/CommonCrypto.h>

using namespace util;

string util::digest::md5(const string & str){
    return move(md5(str.c_str(), str.size()));
}

string util::digest::md5(const void * bytes, size_t sz){
#define hexchar(c) (c < 10 ? '0' + c : 'a' + c - 10)
    unsigned char result[16];

    CC_MD5(bytes, (CC_LONG)sz, result);
    
    char hexstr[33];
    hexstr[32]='\0';
    
    for (int i=0; i<16; i++) {
        unsigned char c = result[i];
        unsigned char cHigh = c >> 4;
        unsigned char cLow = c & 0xf;
        hexstr[2 * i] = hexchar(cHigh);
        hexstr[2 * i + 1] = hexchar(cLow);
    }
    
    return move(hexstr);
#undef hexchar
}