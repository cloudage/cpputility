//
//  ByteArray.cpp
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#include "ByteArray.h"
#include "strings.h"

using namespace util;

ByteArray::ByteArray(size_t sz, const void * raw, bool copy){
    size = sz;
    if(copy){
        bytes = (void *)malloc(sz);
        
        if (raw!=0) {
            memcpy(bytes, raw, sz);
        }else{
            memset(bytes, 0, sz);
        }
    }else{
        bytes = (void *)raw;
    }
}

ByteArray::ByteArray(const string & text):
ByteArray(text.size(),text.c_str(),true){
    
}

ByteArray::ByteArray(const ByteArray * copy)
:ByteArray(copy->size,copy->bytes,true) {

}

ByteArray::~ByteArray(){
    free(bytes);
    size = 0;
}

string ByteArray::makeText() const{
    return move(string((const char *)bytes,size));
}

ByteArray::writer & util::operator<<(ByteArray::writer & writer, const string & val){
    writer.writeValue(val.size());
    writer.writeBytes(val.c_str(), val.size());
    return writer;
}

ByteArray::reader & util::operator>>(ByteArray::reader & reader, string & val){
    size_t sz;
    reader.readValue(sz);
    void* buf = malloc(sz);
    reader.readBytes(buf, sz);
    string ret((char*)buf,sz);
    val = ret;
    free(buf);
    return reader;
}

ByteArray::writer & util::operator<<(ByteArray::writer & writer, const wstring & val){
    return writer<<ws2s(val);
}

ByteArray::reader & util::operator>>(ByteArray::reader & reader, wstring & val){
    string ret;
    reader>>ret;
    val = s2ws(ret);
    return reader;
}