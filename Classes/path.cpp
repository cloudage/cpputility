//
//  path.cpp
//  CppUtility
//
//  Created by 张琪 on 14-5-27.
//
//

#include "path.h"
#include "strings.h"

using namespace util;

path::path(string root,char div){
    if (!root.empty()) {
        segments.push_back(root);
    }
    
    divider = div;
}

path & path::append(const value & sec){
    segments.push_back(trim(sec.asString(),{' ','/','\\'}));
    return refthis;
}

string path::str() const{
    stringstream ss;
    
    for (auto iter = segments.begin();iter!=segments.end();iter++) {
        ss<<divider;
        ss<<*iter;
    }
    
    return move(ss.str());
}

path & path::operator<<(const value & sec){
    return append(sec);
}

path::operator string(){
    return move(str());
}
