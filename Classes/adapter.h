//
//  adapter.hpp
//  CppUtility
//
//  Created by 张 琪 on 15/10/17.
//
//

#ifndef adapter_hpp
#define adapter_hpp

#include "CppUtility.common.h"

namespace util {
    
    template <class T> bool ensure_not_null(T val){ return val != nullptr; }
    
    template <class TSrc, class TDst>
    class adapter {
    public:
        typedef function<TDst(TSrc)> func_create;
        typedef function<bool(TDst)> func_ensure;
        
    private:
        map<TSrc, TDst> _map, _hit;
        func_create _create;
        func_ensure _ensure;
        
    public:
        adapter(func_create create,func_ensure ensure = ensure_not_null<TDst>):
        _create(create),_ensure(ensure){}
        
        void flush(){
            _map = _hit;
            _hit.clear();
        }
        
        TDst get(TSrc src){
            TDst dst = _map[src];
            if (!_ensure(dst)) {
                dst = _create(src);
            }
            
            _hit[src] = dst;
            
            return dst;
        }
        
        template<typename TContainer>
        void sync(const TContainer & container){
            for (auto src : container) {
                get(src);
            }
            
            flush();
        }
    };
}

#endif /* adapter_hpp */
