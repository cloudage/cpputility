//
//  CppUtility.common.h
//  CppUtility
//
//  Created by 张 琪 on 14-9-3.
//
//

#ifndef __CppUtility__CppUtility_common__
#define __CppUtility__CppUtility_common__

#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <queue>
#include <stack>
#include <algorithm>
#include <functional>
#include <thread>
#include <codecvt>
#include <limits>
#include <stdexcept>
#include <memory>

#include <iomanip>

#include <math.h>
#include <sys/timeb.h>
#include <sys/times.h>
#include <assert.h>

#if defined(__SSE3__)
#define VEC_MATH_SSE3
#include <immintrin.h>
#include <stdint.h>
#elif defined(__ARM_NEON__)
#define VEC_MATH_NEON
#include <arm_neon.h>
#else
#define VEC_MATH_NOACC
#warning "no hardware acc on cpu"
#endif

#define TEXT(txt) #txt
#define refthis (*this)

namespace util {
    using namespace std;
}

#endif /* defined(__CppUtility__CppUtility_common__) */
