//
//  spinlock.h
//  util
//
//  Created by 张琪 on 13-9-13.
//
//

#ifndef __util__spinlock__
#define __util__spinlock__

#include "CppUtility.common.h"

#ifdef __APPLE__
#include <libkern/OSAtomic.h>
namespace util {
    class spinlock{
        OSSpinLock _lock = OS_SPINLOCK_INIT;
        
    public:
        inline void lock(){
            OSSpinLockLock(&_lock);
        }
        
        inline bool trylock(){
            return OSSpinLockTry(&_lock);
        }
        
        inline void unlock(){
            OSSpinLockUnlock(&_lock);
        }
        
    };
}
#else
#error "spinlock not implemented on this platform"
#endif

namespace util {
    class lock_guard{
        spinlock & _lock;
    public:
        inline lock_guard(spinlock & lock):_lock(lock){
            _lock.lock();
        }
        
        inline ~lock_guard(){
            _lock.unlock();
        }
    };
}

#define spinlock_guard(lock) util::lock_guard __guard__##lock = lock;
#define spinlock_atomic static util::spinlock __atomic; spinlock_guard(__atomic);

#endif /* defined(__util__spinlock__) */
