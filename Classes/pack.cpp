//
//  pack.cpp
//  CppUtility
//
//  Created by 张 琪 on 14-9-6.
//
//

#include "pack.h"
#include "crypto.h"
#include "strings.h"

using namespace util;
using namespace std;

bool pack::cmp(const void * bytes, size_t sz) const{
    if (_size!=sz) return false;
    
    for (size_t i=0; i<_size; i++) {
        if ( ((unsigned char *)_bytes)[i] != ((unsigned char *)bytes)[i] ) {
            return false;
        }
    }
    
    return true;
}

void pack::set(const void * bytes, size_t sz){
    _size = sz;
    
    if (_size>0) {
        _bytes = malloc(sz);
        memcpy(_bytes, bytes, sz);
    }else{
        _bytes = 0;
    }
    
}

void pack::set(const type_info & ti){
    _type.name = ti.name();
    _type.hash = ti.hash_code();
}

pack::pack(){
    set(0,0);
}

pack::pack(const void * bytes, size_t sz){
    set(bytes,sz);
}

pack::pack(const pack & other){
    set(other._bytes, other._size);
}

pack::~pack(){
    free(_bytes);
}

void pack::operator=(const pack & o){
    set(o._bytes, o._size);
}

bool pack::operator==(const pack & p) const{
    return cmp(p._bytes, p._size);
}