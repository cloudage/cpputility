//
//  dial.h
//  CppUtility
//
//  Created by 张琪 on 14/12/2.
//
//

#ifndef __CppUtility__dial__
#define __CppUtility__dial__

#include "CppUtility.common.h"

namespace util {
    class dial{
        dial * _migration;
        int _current;
        int _min;
        int _max;
        
        void adjust();
        
    public:
        dial(int pmin, int pmax, dial * pmigration=nullptr);
        
        int get() const;
        void add(int val);
        void set(int val);
        
        inline operator int () const { return get(); }
        
        inline void operator += (int val) { add(val); }
        inline void operator -= (int val) { add(-val); }
        inline void operator = (int val) { set(val); }
        inline void operator += (const dial & val) { add(val.get()); }
        inline void operator -= (const dial & val) { add(-val.get()); }
        
        inline bool operator == (int val) const { return get()==val; }
        inline bool operator == (const dial & val) const { return get()==val.get(); }
    };
}

#endif /* defined(__CppUtility__dial__) */
