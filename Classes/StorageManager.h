//
//  StorageManager.h
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#ifndef __CppUtility__StorageManager__
#define __CppUtility__StorageManager__

#include "CppUtility.common.h"
#include "spinlock.h"
#include "rwlock.h"
#include "ByteArray.h"

namespace util {
    struct Storage{
        virtual vector<string> list(const string & path) const =0;
        virtual shared_ptr<ByteArray> read(const string & path) const =0;
        virtual void write(const string & path, const ByteArray * data) =0;
    };
    
    class MemoryStorage:public Storage{
    private:
        map<string, shared_ptr<ByteArray>> _map;
        mutable spinlock _maplock;
    public:
        virtual vector<string> list(const string & path) const override;
        virtual shared_ptr<ByteArray> read(const string & path) const override;
        virtual void write(const string & path, const ByteArray * data) override;
    };
    
    class StorageManager{
    private:
        map<string, shared_ptr<Storage>> _memories;
        
    protected:
        StorageManager() = default;
        
    public:
        static StorageManager * get();
        
        shared_ptr<Storage> getMemory(string name);
        
        virtual shared_ptr<Storage> getCache() =0;
        virtual shared_ptr<Storage> get(string name) =0;
        virtual shared_ptr<Storage> getSystem() =0;
    };
}

#endif /* defined(__CppUtility__Storage__) */
