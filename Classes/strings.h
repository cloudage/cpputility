//
//  strings.h
//  CppUtility
//
//  Created by 张琪 on 14-9-5.
//
//

#ifndef __CppUtility__strings__
#define __CppUtility__strings__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    inline string format(const char * fmt,va_list valst){
        char buf[1024] = {0};
        vsnprintf(buf, 1024, fmt, valst);
        return buf;
    }
    
    inline string format(const char * fmt,...){
        va_list ap;
        va_start(ap, fmt);
        string ret = format(fmt,ap);
        va_end(ap);
        return ret;
    }
    
    inline wstring s2ws(const string & s){
        static wstring_convert<codecvt_utf8<wchar_t>> conv;
        return conv.from_bytes(s);
    }
    
    inline string ws2s(const wstring & ws){
        static wstring_convert<codecvt_utf8<wchar_t>> conv;
        return conv.to_bytes(ws);
    }
    
    vector<string> explode(const string & v, char c);
    
    inline string trim(const string & v, char c=' '){
        string str = v;
        size_t i;
        
        while ((i = str.find(c))!=str.npos) {
            str.erase(str.begin()+i);
        }
        
        return str;
    }
    
    inline string trim(const string & str, const string & chars){
        string ret = str;
        for (char c : chars) {
            ret = trim(ret, c);
        }
        return ret;
    }
    
    bool compare_string_head(const string & str,const string & head);
    bool compare_string_tail(const string & str,const string & tail);
}

#endif /* defined(__CppUtility__strings__) */
