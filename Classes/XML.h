//
//  XML.h
//  CppUtility
//
//  Created by 张琪 on 13-9-12.
//
//

#ifndef __CppUtility__XML__
#define __CppUtility__XML__

#include "CppUtility.common.h"
#include "data_node.h"

namespace util {
    data_node parseXML(string, bool * out_success =0, string * out_errmsg =0);
    string printXML(const data_node &);
}

#endif /* defined(__CppUtility__XML__) */
