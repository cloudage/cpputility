//
//  RCBox.h
//  CppUtility
//
//  Created by 张 琪 on 15/6/7.
//
//

#ifndef __CppUtility__RCBox__
#define __CppUtility__RCBox__

#include "RCObject.h"
#include "value.h"

namespace util {
    class RCBox : public RCObject{
        value _val;
    public:
        RTTI_VISIBLE;
        inline RCBox(value val) { _val = val; }
        inline value get() const { return _val; }
    };
}

#endif /* defined(__CppUtility__RCBox__) */
