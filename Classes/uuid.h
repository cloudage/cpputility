//
//  uuid.h
//  util
//
//  Created by 张琪 on 13-11-12.
//
//

#ifndef __util__uuid__
#define __util__uuid__

#include "CppUtility.common.h"

namespace util {
    using namespace std;
    
    struct uuid{
        union{
            struct{
                uint64_t high,low;
            };
            
            unsigned char bytes[16];
        };
        
        uuid();
        uuid(const char *);
        uuid(const uuid & cp);
        
        inline uuid(const string & str):uuid(str.c_str()) {}
        
        bool operator==(const uuid & other) const;
        bool operator<(const uuid & other) const;
        void operator=(const uuid & cp);
        
        string str() const;
        
        static const uuid zero;
        
    private:
        uuid(bool);
        void init();
    };
}

inline std::string to_string(const util::uuid & id){ return id.str(); }

#endif /* defined(__util__uuid__) */
